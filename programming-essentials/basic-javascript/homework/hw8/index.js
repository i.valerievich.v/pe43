//обработчик событий - он отслеживает действия в браузере , при клике чаще всего.


const price = document.getElementById('price');
const container = document.getElementById('container');

function setPrice() {
    price.addEventListener('focus', (e) => {
        e.target.style.outline = 'none';
        e.target.style.border = '4px solid green';
        e.target.style.borderRadius = '5px';
    },);

    price.addEventListener('blur', (e) => {

        let price = e.target.value;

        const btn = document.createElement('button');
        btn.textContent = 'X';

        const span = document.createElement('span');
        span.style.display = 'block';

        btn.addEventListener("click", deleteSpan);

        function deleteSpan() {
            document.getElementById("price").value = "";
            let span = document.getElementsByTagName('span')[0];
            span.remove();
        }




        if (price <= 0) {
            e.target.style.border = '4px solid red';
            span.innerHTML = 'Please enter correct price';
            container.append(span);
            span.append(btn);
        } else {
            span.innerHTML = `Текущая цена: ${price}`;

            container.prepend(span);
            span.append(btn);

        }


    })

}

setPrice();