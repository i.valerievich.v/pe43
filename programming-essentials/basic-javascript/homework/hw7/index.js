//DOM – позволяет создавать скрипты для динамического доступа к елементам для их обновления, структуры и изменения стилей.

const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const parent = document.body.appendChild(document.createElement('ul'));
const creadCity = (arr, parent = document.body) => {
    return arr.map(item => {
        let li = document.createElement('li');
        parent.appendChild(li);
        li.innerHTML = `${item}`
    })
};
creadCity(arr,parent);


// const array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// let html = '<ul>';
// array.map(function(item) {
//     html = '<li>'+item+'</li>';
// });
// html += '</ul>'
//
// document.write(html);


// const array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// document.write(`<ul>${array.map((item) => `<li>${item}</li>`).join('')}</ul>`);