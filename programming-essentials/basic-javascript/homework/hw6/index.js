// forEach() позволяет выполнить функцию один раз для каждого элемента в массиве в порядке возрастания индекса.








const arr = ['hello', 'world', 23, '23', null];
const userAnswer = prompt("Что вы хотите удалить?", 'string object number');

function filterBy1(arr, userAnswer) {
    return arr.filter(function (item) {
        let type = typeof item;
        return  type !== userAnswer
    })
}

console.log(filterBy1(arr, userAnswer));





function filterBy2(arr, userAnswer) {
    return arr.filter(item => typeof item !== userAnswer)
}

console.log(filterBy2(arr, userAnswer));





const filterBy3 = (arr, userAnswer) => {
   return arr.filter(item => typeof item !== userAnswer)
};

console.log(filterBy3(arr, userAnswer));