### Псевдоклассы

:link
:hover
:active
:focus
:visited

:first-child
:last-child
:nth-child()
:nth-of-type

:not()

### Псевдоэлементы

::after

::before